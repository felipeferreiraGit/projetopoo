#pragma once
#ifndef FORMAGEOMETRICA_H
#define FORMAGEOMETRICA_H
#include <iostream>
#include "Matriz.h"
#include <SDL.h>

class FormaGeometrica {
public:
	// atribui as funções abaixo às classes Retangulo, Triangulo e Circulo
	virtual void desenha(SDL_Renderer* renderer) = 0;
	virtual void desenha(SDL_Renderer * renderer, Matrix& t) = 0;
	virtual void trasformar(Matrix& t) = 0;
	virtual bool estaDentro(int x, int y) = 0;

};

float area(int x1, int y1, int x2, int y2, int x3, int y3);
	

bool testeDentro(int x1, int y1, int x2, int y2, int x3, int y3, int px, int py);
#endif // !FORMAGEOMETRICA_H

