#pragma once
#ifndef TRIANGULO_H
#define TRIANGULO_H
#include "forma_geometrica.h"
#include "Janela.h"
#include "Matriz.h"

class Triangulo : public FormaGeometrica{
public:
	int x1, y1, x2, y2, x3, y3;
public:
	Triangulo(int x1, int y1, int x2, int y2, int x3, int y3);
	virtual void desenha(SDL_Renderer* renderer);
	void desenha(SDL_Renderer* renderer, Matrix& t);
	virtual void trasformar(Matrix& t);
	virtual bool estaDentro(int x, int y);

};


#endif // !TRIANGULO_H
