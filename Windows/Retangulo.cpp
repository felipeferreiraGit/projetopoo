#include "Retangulo.h"
#include "Matriz.h"
#include <iostream>

using namespace std;

Retangulo::Retangulo(int x, int y, int w, int h, int r, int g, int b, int a) :
	pontos(3,4), r(r), g(g), b(b), a(a)
{
	//(ponteiro para double)[posicao]
	(pontos[0])[0] = x;
	pontos[1][0] = y;
	pontos[2][0] = 1;
	pontos[0][1] = x + w;
	pontos[1][1] = y;
	pontos[2][1] = 1;
	pontos[0][2] = x + w;
	pontos[1][2] = y+h;
	pontos[2][2] = 1;
	pontos[0][3] = x;
	pontos[1][3] = y + h;
	pontos[2][3] = 1;

}
void Retangulo::desenha(SDL_Renderer* renderer) {
	// define os pontos de desenho do retângulo
	SDL_Point points[5] = {
			{pontos[0][0], pontos[1][0]},
			{pontos[0][1], pontos[1][1]},
			{pontos[0][2], pontos[1][2]},
			{pontos[0][3], pontos[1][3]},
			{pontos[0][0], pontos[1][0]}
	};

	SDL_RenderDrawLines(renderer, points, 5);
}
void Retangulo::desenha(SDL_Renderer* renderer, Matrix& t){
	// desenho do retângulo, com o uso da matriz tmp
	Matrix tmp = t * pontos;
	SDL_Point points[5] = {
			{tmp[0][0], tmp[1][0]},
			{tmp[0][1], tmp[1][1]},
			{tmp[0][2], tmp[1][2]},
			{tmp[0][3], tmp[1][3]},
			{tmp[0][0], tmp[1][0]}
	};
	
	SDL_SetRenderDrawColor(renderer, r, g, b, a);
	SDL_RenderDrawLines(renderer, points, 5);

}


void Retangulo::trasformar(Matrix& t) {
	// transforma para rotação, translação ou escala
	Matrix tmp = t * pontos;
	pontos = tmp;
	
}
bool Retangulo::estaDentro(int x, int y) {
	//verifica se o ponto está dentro do retangulo
	return testeDentro(pontos[0][0], pontos[1][0], pontos[0][1], pontos[1][1],
		pontos[0][3], pontos[1][3], x, y) || testeDentro(pontos[0][1], pontos[1][1], pontos[0][2], pontos[1][2],
		pontos[0][3], pontos[1][3], x, y);
}



