#include "Janela.h"
#include "Retangulo.h"
#include "Circulo.h"
#include <vector>
#include "Triangulo.h"
#include "Matriz.h"
#include <cstdlib>
#include "transformacao.h"
#include <fstream>
#include <iostream>

using namespace std;

Matrix criaComp(double tx, double ty, double angulo, double sx, double sy) {
	// translacao para a origem
	Matrix T = Transformacao3::translacao(-tx, -ty);
	//criar matriz de rotação
	Matrix R = Transformacao3::rotacao(angulo);
	//criar matriz de escala
	Matrix S = Transformacao3::escala(sx, sy);
	//translação de volta para o ponto do objeto
	Matrix T_inv = Transformacao3::translacao(tx, ty);
	//retornar composta
	return T_inv * R * S * T;
}

int main(int argc, char* argv[]) {
	SDL_Event evento;
	bool continua = false;
	bool selecionado = false;
	Janela _janela("Projeto POO - Editor de Imagens Vetoriais", 800, 600);
	vector <FormaGeometrica*> formas;
	Retangulo retangulo(800 / 2 - 100 / 2, 600 / 2 - 100 / 2, 100, 100, 200, 0, 200, 255);
	Circulo circulo(600, 270, 50);
	Triangulo triangulo(50, 350, 150, 250, 250, 350);

	double angulo = 0;

	formas.push_back(&retangulo);
	formas.push_back(&circulo);
	formas.push_back(&triangulo);

	double t = 0;
	while (!_janela.fechado()) {
		_janela.clear();
		while (SDL_PollEvent(&evento)) {
			int x, y, r;
			int xd, yd, xc = 0, yc = 0;
			int x1, y1, x2, y2, x3, y3;
			switch (evento.type)
			{
				case SDL_QUIT:
					continua = true;
					break;
				case SDL_MOUSEMOTION:
					x = evento.motion.x;
					y = evento.motion.y;
					xd = evento.motion.xrel;
					yd = evento.motion.yrel;
					if (circulo.estaDentro(x, y) && selecionado)
					{
						SDL_GetMouseState(&x, &y);
						circulo.x = x;
						circulo.y = y;
						Matrix T = Transformacao3::translacao(xd, yd);
						circulo.trasformar(T);
					}
					if (retangulo.estaDentro(x, y) && selecionado) {
						SDL_GetMouseState(&x, &y);
						Matrix T = Transformacao3::translacao(xd, yd);
						retangulo.trasformar(T);
					}
					if (triangulo.estaDentro(x, y) && selecionado) {
						SDL_GetMouseState(&x, &y);
						Matrix T = Transformacao3::translacao(xd, yd);
						triangulo.trasformar(T);
					}
					break;
				case SDL_MOUSEBUTTONDOWN:
					x = evento.button.x;
					y = evento.button.y;
					if (circulo.estaDentro(x, y))
					{
						selecionado = true;
						xc = x - circulo.x;
						yc = y - circulo.y;
						Matrix T = Transformacao3::translacao(xc, yc);
						circulo.trasformar(T);
						_janela.mudaCor(0, 255, 0, 255);
						cout << "Dentro do circulo" << endl;
					}
					if (triangulo.estaDentro(x, y)) {
						selecionado = true;
						Matrix R = Transformacao3::rotacao(angulo);
						triangulo.trasformar(R);
						cout << "Dentro do triangulo" << endl;
					}
					if (retangulo.estaDentro(x, y)) {
						selecionado = true;
						Matrix R = Transformacao3::rotacao(angulo);
						retangulo.trasformar(R);
						_janela.mudaCor(0, 255, 0, 255);
						cout << "Dentro do retangulo" << endl;
					}
					break;
				case SDL_MOUSEBUTTONUP:
					if (evento.button.button == SDL_BUTTON_LEFT)
						selecionado = false;
					break;
			}
		}
		_janela.clear();
		for (int f = 0; f < formas.size(); f++) {
			formas[f]->desenha(_janela._renderer);
		}
		t += 0.01;
		_janela.atuaiza_tela();
	}

	return 0;
}
