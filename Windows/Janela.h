#pragma once
#ifndef JANELA_H
#define JANELA_H
#include <string>
#include <SDL.h>

using namespace std;

class Janela {
public:
	Janela(const string &titulo, int largura, int altura);
	~Janela();

	void clear() const;
	void atuaiza_tela();
	void mudaCor(Uint8 r, Uint8 g, Uint8 b, Uint8 a);
	inline bool fechado() const { return fechar; }

private:
	bool init();

private:
	string _titulo;
	int Largura = 800;
	int Altura = 600;

	bool fechar = false;

	SDL_Window* _janela = NULL;

public:
	SDL_Renderer *_renderer = NULL;
};
#endif // !JANELA_H

