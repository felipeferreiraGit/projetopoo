#include "Janela.h"
#include <iostream>

using namespace std;

Janela::Janela(const string& titulo, int largura, int altura):
	_titulo(titulo), Largura(largura), Altura(altura)
{
	if (!init()) {
		fechar = true;
	}
	fechar = !init();
}

Janela::~Janela() {
	SDL_DestroyRenderer(_renderer);
	SDL_DestroyWindow(_janela);
	SDL_Quit();
}
bool Janela::init() {
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		cout << "Falha na inicialização! \n";
		return 0;
	}
	_janela = SDL_CreateWindow(_titulo.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, Largura, Altura, 0);

	if (_janela == nullptr) {
		cout << "Falha na criação da janela! \n";
		return 0;
	}
	_renderer = SDL_CreateRenderer(_janela, -1, SDL_RENDERER_ACCELERATED);
	if (_renderer == nullptr) {
		cout << "Falha na renderização!\n";
		return 0;
	}

	return true;
}


void Janela::mudaCor(Uint8 r, Uint8 g, Uint8 b, Uint8 a) {

	r = 255;
	g = 0;
	b = 0;
	a = 255;

	SDL_SetRenderDrawColor(_renderer, r, g, b, a);
}

void Janela::clear() const {
	Uint8 r, g, b, a;
	SDL_GetRenderDrawColor(_renderer, &r, &g, &b, &a);
	SDL_SetRenderDrawColor(_renderer, 0, 0, 0, 255);
	SDL_RenderClear(_renderer);
	SDL_SetRenderDrawColor(_renderer, r, g, b, a);
}
void Janela::atuaiza_tela() {
	SDL_RenderPresent(_renderer);
}
