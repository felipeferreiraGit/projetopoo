#pragma once
#ifndef  TRANSFORMACAO_H
#define TRANSFORMACAO_H
#include "Matriz.h"

class Transformacao3 {
public:
	Transformacao3()
	{

	}
	static Matrix rotacao(double angulo) {
		// implementa a rotação na figura selecionada
		Matrix R(3, 3);
		angulo += M_PI / 60.0f;
		R.a[0][0] = cos(angulo);
		R.a[0][1] = -sin(angulo);
		R.a[1][0] = sin(angulo);
		R.a[1][1] = cos(angulo);
		R.a[2][2] = 1;

		return R;
	}
	static Matrix escala(double sx, double sy) {
		Matrix S(3, 3);
		S[0][0] = sx;
		S[1][1] = sy;
		S[2][2] = 1;

		return S;
	}
	static Matrix translacao(double tx, double ty) {
		// executa a translação da figura selecionada
		Matrix T = Matrix::identidade(3);
		T.a[0][2] = tx;
		T.a[1][2] = ty;

		return T;
	}
};

#endif // ! TRANSFORMACAO_H
