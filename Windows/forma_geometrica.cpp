#include "forma_geometrica.h"
#include <cmath>


float area(int x1, int y1, int x2, int y2, int x3, int y3) {
	return abs((x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2);
}
bool testeDentro(int x1, int y1, int x2, int y2, int x3, int y3, int px, int py) {
	float a = area(x1, y1, x2, y2, x3, y3);   // calculando ABC
	float A1 = area(px, py, x2, y2, x3, y3);  // calculando PBC
	float A2 = area(x1, y1, px, py, x3, y3);  // calculando APC
	float A3 = area(x1, y1, x2, y2, px, py);  // calculando ABP
	return (a == A1 + A2 + A3);
}
