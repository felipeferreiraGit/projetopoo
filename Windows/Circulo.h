#pragma once
#ifndef CIRCULO_H
#define CIRCULO_H

#include "Janela.h"
#include "forma_geometrica.h"

using namespace std;

class Circulo : public FormaGeometrica {
public:
	Matrix pontos, rx, ry;
	int x, y, r, nlados;
public:
	Circulo(int x, int y, int r, int nlados = 30);
	virtual void desenha(SDL_Renderer* renderer);
	void desenha(SDL_Renderer* renderer, Matrix& t);
	virtual void trasformar(Matrix& t);
	virtual bool estaDentro(int x, int y);

	
};

#endif // !CIRCULO_H
