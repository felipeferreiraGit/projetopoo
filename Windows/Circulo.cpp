#include "Circulo.h"
#include "Matriz.h"
#include <iostream>
#include <cmath>

using namespace std;

Circulo::Circulo(int x, int y, int r, int nlados) :
    pontos(3, nlados), rx(3, 1), ry(3, 1), x(x), y(y), r(r), nlados(nlados)
{
    for (int i = 0; i < nlados; i++) {
        pontos[0][i] = x + r * cos(i * (2 * M_PI / nlados));
        pontos[1][i] = y + r * sin(i * (2 * M_PI / nlados));
        pontos[2][i] = 1;
    }
    rx[0][0] = x+r;
    rx[1][0] = 0;
    rx[2][0] = 1;
    ry[0][0] = 0;
    ry[1][0] = y+r;
    ry[2][0] = 1;
}

void Circulo::desenha(SDL_Renderer* renderer) {
    
    // desenho do circulo
    for (int i = 0; i < nlados-1; i++) {
        SDL_RenderDrawLine(renderer, pontos[0][i], pontos[1][i],
            pontos[0][i+1], pontos[1][i+1]);
       
    }
    SDL_RenderDrawLine(renderer, pontos[0][nlados-1], pontos[1][nlados-1],
        pontos[0][0], pontos[1][0]);
}


void Circulo::desenha(SDL_Renderer* renderer, Matrix& t) {
    // transformando o x
    Matrix U = t * pontos;
    for (int i = 0; i < nlados - 1; i++) {
        SDL_RenderDrawLine(renderer, U[0][i], U[1][i],
            U[0][i + 1], U[1][i + 1]);
    }
    SDL_RenderDrawLine(renderer, U[0][nlados - 1], U[1][nlados - 1],
        U[0][0], U[1][0]);
}

void Circulo::trasformar(Matrix& t) {
    Matrix U = t * pontos;
    pontos = U;

    Matrix X(3, 1);
    X[0][0] = x;
    X[1][0] = y;
    X[2][0] = 1;
    U = t * X;
    x = U[0][0];
    y = U[1][0];

    U = t * rx;
    rx = U;
    U = t * ry;
    ry = U;
    
}
bool Circulo::estaDentro(int x, int y) {
    // verifica se o ponto está dentro do circulo
    cout << x << ", " << y << endl;
    cout << Circulo::x << ", " << Circulo::y << endl;
    cout << rx[0][0] << ", " << rx[0][1] << endl;

    return sqrt(pow(Circulo::x - x, 2) + pow(Circulo::y - y, 2)) <= (rx[0][0] - x);
}

