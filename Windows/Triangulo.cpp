#include "Triangulo.h"
#include "Matriz.h"
#include <iostream>
#define POINTS_COUNT 4

using namespace std;

Triangulo::Triangulo(int x1, int y1, int x2, int y2, int x3, int y3) :
x1(x1), y1(y1), x2(x2), y2(y2), x3(x3), y3(y3)
{

}
void Triangulo::desenha(SDL_Renderer* renderer) {
	SDL_Point points[POINTS_COUNT] = {
		{x1, y1},
		{x2, y2},
		{x3, y3},
		{x1, y1}
	};
	SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
	SDL_RenderDrawLines(renderer, points, 4);
}

void Triangulo::desenha(SDL_Renderer* renderer, Matrix& t) {
	// desenha o triângulo com os pontos x1, y1, x2, y2, x3, y3
	SDL_Point points[POINTS_COUNT] = {
		{x1, y1},
		{x2, y2},
		{x3, y3},
		{x1, y1}
	};
	SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
	SDL_RenderDrawLines(renderer, points, 4);
}
void Triangulo::trasformar(Matrix& t) {
	//implementa a matriz de rotação no triângulo
	Matrix p(3, 3);
	p.a[0][0] = x1;
	p.a[1][0] = y1;
	p.a[2][0] = 1;
	p.a[0][1] = x2;
	p.a[1][1] = y2;
	p.a[2][1] = 1;
	p.a[0][2] = x3;
	p.a[1][2] = y3;
	p.a[2][2] = 1;

	Matrix tmp = t*p;
	x1 = tmp.a[0][0];
	y1 = tmp.a[1][0];
	x2 = tmp.a[0][1];
	y2 = tmp.a[1][1];
	x3 = tmp.a[0][2];
	y3 = tmp.a[1][2];
	p = tmp;
	p.print_matrix();
}

bool Triangulo::estaDentro(int x, int y) {
	// verifica se o ponto está dentro do triangulo
	return (testeDentro(x1, y1, x2, y2, x3, y3, x, y));
		
	
}
