#pragma once
#ifndef MATRIZ_H
#define MATRIZ_H
#include <iostream>
#include <cassert>

using namespace std;

class Matrix {
    unsigned int rows;
    unsigned int columns;

    void alloc(unsigned int rows, unsigned columns) {
        free();
        // aloca a memória para armazenar os valores na matriz
        Matrix::rows = rows;
        Matrix::columns = columns;
        a = new double* [rows];
        for (unsigned int r = 0; r < rows; r++) {
            a[r] = new double[columns];
            // inicializa com o valor padrão 0
            for (unsigned int c = 0; c < columns; c++)
                a[r][c] = 0;
        }
    }
    void free() {
        if (a != nullptr) {
            // libera memória
            for (unsigned int r = 0; r < rows; r++)
                delete[] a[r];
            delete[] a;
            a = nullptr;
        }
        rows = 0;
        columns = 0;
    }
    void copy(const Matrix& m) {
        for (unsigned int r = 0; r < rows; r++)
            for (unsigned int c = 0; c < columns; c++)
                a[r][c] = m.a[r][c];
    }
public:
    double** a;
    Matrix() :
        Matrix(0, 0) { }
    Matrix(unsigned int rows, unsigned columns) :
        rows{ 0 }, columns{ 0 }, a{ nullptr } {
        alloc(rows, columns);
    }
    Matrix(const Matrix& m) :
        Matrix(m.rows, m.columns) {
        // faz a cópia dos elementos de m
        copy(m);
    }
    ~Matrix() {
        free();
    }
    Matrix& operator=(const Matrix& m) {
        // aloca a memória coerente com o tamanho de linhas e colunas
        // de m
        alloc(m.rows, m.columns);
        // copia os elementos de m
        copy(m);
        return *this;
    }
    Matrix& operator*=(const Matrix& m) {
        assert(columns == m.rows);
        Matrix tmp(rows, m.columns);
        for (unsigned int r = 0; r < rows; r++)
            for (unsigned int c = 0; c < m.columns; c++) {
                tmp.a[r][c] = 0;
                for (unsigned int k = 0; k < m.rows; k++)
                    tmp.a[r][c] += a[r][k] * m.a[k][c];
            }
        free();
        Matrix::rows = tmp.rows;
        Matrix::columns = tmp.columns;
        
        Matrix::a = tmp.a;
        tmp.a = nullptr;
        tmp.rows = 0;
        tmp.columns = 0;

        return *this;
    }
    Matrix operator*(const Matrix& m) {
        assert(columns == m.rows);
        Matrix tmp(*this);
        tmp *= m;

        return tmp;
    }
    void print_matrix() {
        // mostra/desenha a matriz
        cout << "rows: " << rows << endl;
        cout << "columns: " << columns << endl;
        cout << "a: " << a << endl;
        for (unsigned int r = 0; r < rows; r++) {
            for (unsigned int c = 0; c < columns - 1; c++)
                cout << a[r][c] << " ";
            if (columns > 0)
                cout << a[r][columns - 1];
            cout << endl;
        }
    }
    double * operator[](int pos) {

        return a[pos];
    }
    static Matrix nula(int linhas, int colunas) {
        return Matrix(linhas, colunas);
    }
    static Matrix identidade(int linhas) {
        Matrix r = nula(linhas, linhas);
        for (int i = 0; i < linhas; i++) {
            r.a[i][i] = 1.0f;
        }
        return r;
    }
};

#endif // !MATRIZ_H

