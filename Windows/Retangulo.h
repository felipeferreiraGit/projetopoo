#pragma once
#ifndef RETANGULO_H
#define RETANGULO_H
#include "Janela.h"
#include "forma_geometrica.h"
#include "Matriz.h"

using namespace std;

class Retangulo : public FormaGeometrica {
public:
	Retangulo(int w, int h, int x, int y, int r, int g, int b, int a);
	virtual void desenha(SDL_Renderer* renderer);
	void desenha(SDL_Renderer* renderer, Matrix& t);
	virtual void trasformar(Matrix& transformacao);
	virtual bool estaDentro(int x, int y);
public:
	Matrix pontos;
	int r, g, b, a;
};


#endif
